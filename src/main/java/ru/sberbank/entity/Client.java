package ru.sberbank.entity;

import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "clients")
@EqualsAndHashCode
public class Client {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id_client;
    private String lastName;
    private String firstName;
    private String middleName;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "CLIENT_PASSPORT_REL",
            joinColumns = @JoinColumn(name = "id_client"),
            inverseJoinColumns = {
                    @JoinColumn(name = "serial"),
                    @JoinColumn(name = "number")})
    private Passport passport;
    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "client")
    private List<Transfer> transfers = new ArrayList<>();


    public Client() {

    }

    public Client(String lastName, String firstName, String middleName, Passport passport) {
        id_client = UUID.randomUUID().toString();
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.passport = passport;

    }

    public Client(String id_client, String lastName, String firstName, String middleName, Passport passport) {
        this.id_client = id_client;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.passport = passport;
    }

    public String getId_client() {
        return id_client;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Passport getPassport() {
        return passport;
    }

    public void setPassport(Passport passport) {
        this.passport = passport;
    }

    public void addRecipient(Transfer transfer) {
        transfers.add(transfer);
    }

    public List<Transfer> getTransfers() {
        return transfers;
    }

    @Override
    public String toString() {
        return firstName + ' ' +
                lastName + ' ' +
                middleName + ' ' +
                passport.toString();
    }
}
