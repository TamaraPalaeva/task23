package ru.sberbank.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class PassportKey implements Serializable {
    @Column(name = "serial")
    private int serial;
    @Column(name = "number")
    private int number;

    public PassportKey() {
    }

    public PassportKey(int seria, int number) {
        this.serial = seria;
        this.number = number;
    }

    public int getSeria() {
        return serial;
    }

    public void setSeria(int seria) {
        this.serial = seria;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "seria=" + serial +
                ", number=" + number;
    }
}
