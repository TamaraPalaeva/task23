package ru.sberbank.entity;

import javax.persistence.*;

@Entity
@Table(name = "passports")
public class Passport {

    @EmbeddedId
    PassportKey passportKey;
    private String info;
    @OneToOne(mappedBy = "passport")
    private Client client;

    public Passport() {
    }

    public Passport(PassportKey passportKey, String info) {
        this.passportKey = passportKey;
        this.info = info;
    }

    public PassportKey getPassportKey() {
        return passportKey;
    }

    public void setPassportKey(PassportKey passportKey) {
        this.passportKey = passportKey;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Client getClient() {
        return client;
    }

    @Override
    public String toString() {
        return "Passport{" +
                passportKey +
                ", info='" + info + '}';
    }
}
