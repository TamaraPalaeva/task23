package ru.sberbank.entity;

import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "transfers")
@EqualsAndHashCode
public class Transfer {
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id_transfer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client",
            foreignKey = @ForeignKey(name = "id_client"))
    private Client client;


    private int summ;

    public Transfer() {
    }

    public Transfer(Client recipientClient, int summ) {
        this.id_transfer = UUID.randomUUID().toString();
        this.client = recipientClient;
        this.summ = summ;

    }


    public Transfer(String id_transfer, Client recipientClient, int summ) {
        this.id_transfer = id_transfer;
        this.client = recipientClient;
        this.summ = summ;
    }

    public Client getRecipientClient() {
        return client;
    }

    public void setRecipientClient(Client recipientClient) {
        this.client = recipientClient;
        recipientClient.addRecipient(this);
    }


    public int getSumm() {
        return summ;
    }

    public void setSumm(int summ) {
        this.summ = summ;
    }


    @Override
    public String toString() {
        return "Transfer{" +
                "Client=" + client.toString() +
                ", summ=" + summ +
                '}';
    }
}
