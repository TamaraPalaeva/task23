package ru.sberbank.springData;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.sberbank.entity.Client;
import ru.sberbank.entity.Transfer;
import ru.sberbank.springData.repository.ClientService;
import ru.sberbank.springData.repository.TransferServise;

import java.util.List;

public class App {

    public static void main(String[] args) {


        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        ClientService clientService = applicationContext.getBean(ClientService.class);
        TransferServise transferServise = applicationContext.getBean(TransferServise.class);
//        Client client = new Client("Рогов", "Роман", "Федорович",
//                new Passport(new PassportKey(3214, 124554), "ОВД Минского района"));
//
//        clientService.save(client);


        List<Client> clients = clientService.findAll();
        for (Client rec : clients) {
            System.out.println(rec.toString());
        }

        //      transferServise.save(new PassportKey(3214, 124554), 12000);

        List<Transfer> transfers = transferServise.findAll();
        for (Transfer transfer : transfers) {
            System.out.println(transfer.toString());
        }


    }
}
