package ru.sberbank.springData.repository.impl;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sberbank.entity.Client;
import ru.sberbank.entity.PassportKey;

import java.util.List;


@Repository
public interface ClientRepository extends CrudRepository<Client, String> {

    List<Client> findAll();

    @Query("select c from Client c where c.passport.passportKey = ?1")
    Client findDistinctClientByPassport_PassportKey(PassportKey passportKey);

}
