package ru.sberbank.springData.repository.impl;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.sberbank.entity.Transfer;

import java.util.List;

public interface TransferRepository extends CrudRepository<Transfer, String> {

    @Query("select t from Transfer t")
    List<Transfer> findAll();


}
