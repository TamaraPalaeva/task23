package ru.sberbank.springData.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.entity.Client;
import ru.sberbank.entity.PassportKey;
import ru.sberbank.springData.repository.impl.ClientRepository;

import java.util.List;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    public void save(Client client) {
        clientRepository.save(client);
    }

    public Client findByPassportKey(PassportKey passportKey) {
        return clientRepository.findDistinctClientByPassport_PassportKey(passportKey);
    }
}
