package ru.sberbank.springData.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.entity.Client;
import ru.sberbank.entity.PassportKey;
import ru.sberbank.entity.Transfer;
import ru.sberbank.springData.repository.impl.ClientRepository;
import ru.sberbank.springData.repository.impl.TransferRepository;

import java.util.List;

@Service
public class TransferServise {
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private TransferRepository transferRepository;

    public List<Transfer> findAll() {
        return transferRepository.findAll();
    }

    public void save(PassportKey recipientKey, int summ) {
        Client recipient = clientRepository.findDistinctClientByPassport_PassportKey(recipientKey);

        transferRepository.save(new Transfer(recipient, summ));
    }
}
