package ru.sberbank.hibernate.dao;


import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.sberbank.entity.Transfer;
import ru.sberbank.hibernate.HibernateSessionFactory;

import java.util.List;

public class TransferDAO implements EntityDAO<Transfer>{

    public Transfer findById(String id) {
        return HibernateSessionFactory.getSessionFactory().openSession().get(Transfer.class, id);
    }

    public void save(Transfer transfer) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(transfer);
        transaction.commit();
        session.close();
    }

    public void update(Transfer transfer) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(transfer);
        tx1.commit();
        session.close();
    }

    public void delete(Transfer transfer) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(transaction);
        transaction.commit();
        session.close();
    }

    public List<Transfer> findAll() {
        return (List<Transfer>) HibernateSessionFactory.getSessionFactory().openSession()
                .createQuery("FROM Transfer ").list();
    }
}
