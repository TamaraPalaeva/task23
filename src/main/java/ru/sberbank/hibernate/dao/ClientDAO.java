package ru.sberbank.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.sberbank.entity.Client;
import ru.sberbank.entity.PassportKey;
import ru.sberbank.hibernate.HibernateSessionFactory;

import java.util.List;

public class ClientDAO implements EntityDAO<Client>{

    public Client findById(String id) {
        return HibernateSessionFactory.getSessionFactory().openSession().get(Client.class, id);
    }

    public Client findByPassport(PassportKey passportKey) {
        Query<Client> query = HibernateSessionFactory.getSessionFactory().openSession()
                .createQuery("select c from Passport p INNER JOIN p.client c WHERE p.passportKey = :passportKey", Client.class);
        query.setParameter("passportKey", passportKey);
        Client client = query.getSingleResult();
        return client;
    }

    public void save(Client client) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.save(client);
        transaction.commit();
        session.close();
    }

    public void update(Client client) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(client);
        tx1.commit();
        session.close();
    }

    public void delete(Client client) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(client);
        transaction.commit();
        session.close();
    }

    public List<Client> findAll() {
        return (List<Client>) HibernateSessionFactory.getSessionFactory().openSession()
                .createQuery("FROM Client").list();
    }
}
