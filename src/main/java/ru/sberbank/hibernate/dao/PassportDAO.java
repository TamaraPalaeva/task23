package ru.sberbank.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.sberbank.entity.Client;
import ru.sberbank.entity.Passport;
import ru.sberbank.entity.PassportKey;
import ru.sberbank.hibernate.HibernateSessionFactory;

public class PassportDAO implements EntityDAO<Passport>{

    public void save(Passport passport) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(passport);
        transaction.commit();
        session.close();
    }

    public void update(Passport passport) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(passport);
        transaction.commit();
        session.close();
    }

    public void delete(Passport passport) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction= session.beginTransaction();
        session.delete(passport);
        transaction.commit();
        session.close();
    }

    public Passport find(PassportKey passportKey) {
        return HibernateSessionFactory.getSessionFactory().openSession().get(Passport.class, passportKey);
    }

}
