package ru.sberbank.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.sberbank.entity.Client;
import ru.sberbank.hibernate.HibernateSessionFactory;

import java.util.List;

public interface EntityDAO<T> {
    public void save(T t) ;

    public void update(T t) ;

    public void delete(T t) ;
}
