package ru.sberbank.hibernate;

import ru.sberbank.entity.Client;
import ru.sberbank.entity.Passport;
import ru.sberbank.entity.PassportKey;
import ru.sberbank.entity.Transfer;
import ru.sberbank.hibernate.service.ClientService;
import ru.sberbank.hibernate.service.PassportService;
import ru.sberbank.hibernate.service.TransferService;

import java.util.List;

public class AppHibernate {
    public static void main(String[] args) {
        ClientService clientService = new ClientService();
        PassportService passportService = new PassportService();
        TransferService transferService = new TransferService();

        PassportKey passportKey1 = new PassportKey(3613,789632);
        Passport passport1 = new Passport(passportKey1, "УВД Промышленного района") ;
        Client client1 = new Client("Иванов", "Иван", "Николаевич", passport1);
        passportService.save(passport1);
        clientService.save(client1);

        PassportKey passportKey2 = new PassportKey(3613,789954);
        Passport passport2 = new Passport(passportKey2, "УВД Промышленного района") ;
        Client client2 = new Client("Петров", "Алексей", "Васильевич", passport2);
        passportService.save(passport2);
        clientService.save(client2);

        transferService.save(passportKey1, 21453);

        clientService.update(client1);
        clientService.update(client2);

        Client client = clientService.findByPassport(passportKey1);

        Passport passport = passportService.find(passportKey1);

        System.out.println(passport.toString());
        System.out.println(client.toString());

        List<Transfer> transfers = transferService.findAll();
        for (Transfer transfer : transfers) {
            System.out.println(transfer.toString());
        }


    }
}
