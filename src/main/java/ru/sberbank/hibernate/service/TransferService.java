package ru.sberbank.hibernate.service;

import ru.sberbank.entity.Client;
import ru.sberbank.entity.PassportKey;
import ru.sberbank.entity.Transfer;
import ru.sberbank.hibernate.dao.TransferDAO;

import java.util.List;

public class TransferService implements ServiceDAO<Transfer> {

    TransferDAO transferDAO = new TransferDAO();

    public void save(PassportKey recipientKey, int summ) {
        Client recipient = new ClientService().findByPassport(recipientKey);
        Transfer transfer = new Transfer(recipient, summ);
        transferDAO.save(transfer);
        recipient.addRecipient(transfer);
    }

    @Override
    public void delete(Transfer transfer) {
        transferDAO.delete(transfer);
    }

    @Override
    public void update(Transfer transfer) {
        transferDAO.update(transfer);
    }

    public Transfer findById(String id){
        return transferDAO.findById(id);
    }

    public List<Transfer> findAll(){
        return transferDAO.findAll();
    }


}
