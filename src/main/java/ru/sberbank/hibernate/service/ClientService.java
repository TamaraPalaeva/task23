package ru.sberbank.hibernate.service;

import ru.sberbank.entity.Client;
import ru.sberbank.entity.PassportKey;
import ru.sberbank.hibernate.dao.ClientDAO;

import java.util.List;

public class ClientService implements ServiceDAO<Client>{

    private ClientDAO clientDAO = new ClientDAO();

    public Client find(String id) {
        return clientDAO.findById(id);
    }

    public Client findByPassport (PassportKey passportKey){
        return clientDAO.findByPassport(passportKey);
    }

    public void save(Client client) {
        clientDAO.save(client);
    }

    public void delete(Client client) {
        clientDAO.delete(client);
    }

    public void update(Client client) {
        clientDAO.update(client);
    }

    public List<Client> findAll() {
        return clientDAO.findAll();
    }
}
