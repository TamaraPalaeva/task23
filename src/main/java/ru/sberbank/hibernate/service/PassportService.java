package ru.sberbank.hibernate.service;

import ru.sberbank.entity.Passport;
import ru.sberbank.entity.PassportKey;
import ru.sberbank.hibernate.dao.PassportDAO;

public class PassportService implements ServiceDAO<Passport> {

    PassportDAO passportDAO = new PassportDAO();


    public void save(Passport passport) {
        passportDAO.save(passport);
    }

    @Override
    public void delete(Passport passport) {
        passportDAO.delete(passport);
    }

    @Override
    public void update(Passport passport) {
        passportDAO.update(passport);

    }

    public Passport find(PassportKey passportKey){
        return passportDAO.find(passportKey);
    }
}
