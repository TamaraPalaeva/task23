package ru.sberbank.hibernate.service;

public interface ServiceDAO<T> {

    public void delete(T t) ;

    public void update(T t) ;
}
