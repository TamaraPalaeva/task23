  Создайте сущности Client, Passport, Transfer (перевод). Отношение между клиентом и паспортом двунаправленное один к 
  одному, способ загрузки EAGER, между клиентом и переводом однонаправленное один ко многим, способ  загрузки LAZY. 
      1) Реализуйте доступ к сущностям, используя возможности Hibernate (какой удобно способ: JPQL,SQL или Criteria API),
     через создание DAO-классов;
     2) то же что в пункте 1, но используя возможности Spring Data (вместо DAO-классов будут классы Repository).
     
  Добавьте возможность получать клиента по его паспортным данным, все переводы клиента по его паспортным данным.

     Зависимости Hibernate:
    <dependency>
        <groupId>org.hibernate</groupId>
        <artifactId>hibernate-core</artifactId>
    </dependency>
    <dependency>
        <groupId>org.hibernate</groupId>
        <artifactId>hibernate-entitymanager</artifactId>
    </dependency>

     Зависимости Spring Data:
    <dependency>
        <groupId>org.springframework.data</groupId>
        <artifactId>spring-data-jpa</artifactId>
    </dependency>